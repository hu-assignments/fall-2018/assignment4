from sys import argv as args
import re


class ArgumentHandler:
    class InvalidArgumentException(Exception):
        def __init__(self, message):
            self.message = message

    def __init__(self, arguments: list):
        self.arguments = arguments
        if len(self.arguments) != 2:
            raise ArgumentHandler.InvalidArgumentException("One argument needed for input filename, you entered {}.".format(len(self.arguments) - 1))

    @property
    def filename(self) -> str:
        return self.arguments[1]


class FileHandler:
    def __init__(self, filename: str):
        self.filename = filename

    @property
    def raw_file_content(self):
        with open(self.filename, "r") as input_file:
            return input_file.read()


class Board:
    def __init__(self, board_matrix: list):
        self.matrix = board_matrix
        self.number_of_cleaned_cells = 0

    class NoNeighbourException(Exception):
        def __init__(self, message):
            self.message = message

    @staticmethod
    def get_upper_position(position):
        return position[0] - 1, position[1]

    @staticmethod
    def get_lower_position(position):
        return position[0] + 1, position[1]

    @staticmethod
    def get_left_position(position):
        return position[0], position[1] - 1

    @staticmethod
    def get_right_position(position):
        return position[0], position[1] + 1

    def try_getting_cell(self, position):
        try:
            if position[0] < 0 or position[1] < 0:
                raise IndexError()
            return self.get_cell(position) if self.get_cell(position) != " " else None
        except IndexError:
            return None

    def get_upper_cell(self, position):
        return self.try_getting_cell(Board.get_upper_position(position))

    def get_lower_cell(self, position):
        return self.try_getting_cell(Board.get_lower_position(position))

    def get_left_cell(self, position):
        return self.try_getting_cell(Board.get_left_position(position))

    def get_right_cell(self, position):
        return self.try_getting_cell(Board.get_right_position(position))

    def get_cell(self, position):
        return self.matrix[position[0]][position[1]]

    def clean_for_position(self, position, checking_neighbours=False):
        if not checking_neighbours and self.no_neighbours_for_initial_position(position) and self.try_getting_cell(position):
            raise Board.NoNeighbourException(
                "The cell {} with position {} has no neighbour with same value.".format(self.get_cell(position), (position[0] + 1, position[1] + 1)))
        elif self.try_getting_cell(position) is not None:
            current_cell = self.get_cell(position)
            self.matrix[position[0]][position[1]] = " "
            self.number_of_cleaned_cells += 1

            upper_cell = self.get_upper_cell(position)
            if current_cell == upper_cell:
                self.clean_for_position(Board.get_upper_position(position), checking_neighbours=True)

            lower_cell = self.get_lower_cell(position)
            if current_cell == lower_cell:
                self.clean_for_position(Board.get_lower_position(position), checking_neighbours=True)

            left_cell = self.get_left_cell(position)
            if current_cell == left_cell:
                self.clean_for_position(Board.get_left_position(position), checking_neighbours=True)

            right_cell = self.get_right_cell(position)
            if current_cell == right_cell:
                self.clean_for_position(Board.get_right_position(position), checking_neighbours=True)

    def sink_cells(self):
        row_width = len(self.matrix[0])
        column_width = len(self.matrix)
        columns = [[self.matrix[row_i][col_i] for row_i in range(len(self.matrix))] for col_i in range(row_width)]

        for i in range(len(columns)):
            columns[i] = [columns[i][j] for j in range(len(columns[i])) if columns[i][j] != " "]
            columns[i] = [" " for _ in range(column_width - len(columns[i]))] + columns[i]

        self.matrix = [[columns[i][j] for i in range(len(columns))] for j in range(column_width)]

    def shrink_matrix(self):
        row_width = len(self.matrix[0])
        column_width = len(self.matrix)
        columns = [[self.matrix[r][c] for r in range(len(self.matrix))] for c in range(row_width)]

        columns_to_be_deleted = []
        for c in range(len(columns)):
            if columns[c].count(" ") == len(columns[c]):
                columns_to_be_deleted.append(c)

        self.matrix = [[columns[i][j] for i in range(len(columns)) if i not in columns_to_be_deleted] for j in range(column_width)]

        rows_to_be_deleted = []
        for r in range(len(self.matrix)):
            if self.matrix[r].count(" ") == len(self.matrix[r]):
                rows_to_be_deleted.append(r)

        self.matrix = [self.matrix[r] for r in range(len(self.matrix)) if r not in rows_to_be_deleted]

    def get_matched_neighbours(self, position):
        initial_cell = self.get_cell(position)
        left = self.get_left_cell(position)
        right = self.get_right_cell(position)
        up = self.get_upper_cell(position)
        down = self.get_lower_cell(position)
        return [item for item in (up, right, down, left) if item == initial_cell]

    def no_neighbours_for_initial_position(self, position) -> bool:
        return len([item for item in self.get_matched_neighbours(position) if item != " "]) == 0

    def __str__(self):
        return ("\n" + " " * Output.Prefix.PADDING).join([str(row)[1:-1].replace(", ", " ").replace("\'", "") for row in self.matrix])


class Game:
    def __init__(self, board: Board):
        self.board = board
        self.score = 0

    def play(self, position: tuple):
        adjusted_pos = (position[0] - 1, position[1] - 1)
        selected_cell = self.board.get_cell(adjusted_pos)
        self.board.clean_for_position(adjusted_pos)
        self.board.sink_cells()
        self.board.shrink_matrix()
        self.scoring(int(selected_cell))

    def scoring(self, cell_value: int):
        self.score += cell_value * Game.fibonacci(self.board.number_of_cleaned_cells)
        self.board.number_of_cleaned_cells = 0

    def cleanup(self):
        self.board.number_of_cleaned_cells = 0

    @staticmethod
    def fibonacci(n):
        a, b = 0, 1
        for _ in range(0, n):
            a, b = b, a + b
        return a

    def start_game(self):
        Output.print(str(self.board), prefix=Output.Prefix.BOARD)
        Output.print("Your score is {}.".format(self.score), prefix=Output.Prefix.INFO)
        while not self.game_over():
            inp = str(input("\n[INPUT] Enter a row and a column number: "))
            if re.fullmatch(r"([ ]+|)\d+[ ]+\d+([ ]+|)", inp):
                pos = int(inp.split()[0]), int(inp.split()[1])
                try:
                    self.play(pos)
                    self.print_board_n_score()
                except (IndexError, ValueError):
                    Output.print("Entered position is non-existent, please enter a valid position.", prefix=Output.Prefix.ERROR)
                    self.cleanup()
                except Board.NoNeighbourException as nne:
                    Output.print(nne.message, prefix=Output.Prefix.INFO)
                    self.print_board_n_score()
            else:
                Output.print("Please enter position in correct format: row column", prefix=Output.Prefix.ERROR)
        else:
            Output.print("Game is over. Total score: {}".format(self.score), prefix=Output.Prefix.INFO)

    def game_over(self):
        for r in range(len(self.board.matrix)):
            for c in range(len(self.board.matrix[r])):
                if not self.board.no_neighbours_for_initial_position((r, c)):
                    return False
        return True

    def print_board_n_score(self):
        Output.print(str(self.board), prefix=Output.Prefix.BOARD)
        Output.print("Your score is {}.".format(self.score), prefix=Output.Prefix.INFO)


class Output:
    class Prefix:
        PADDING = 8
        BOARD = "[BOARD]".ljust(PADDING)
        INPUT = "[INPUT]".ljust(PADDING)
        ERROR = "[ERROR]".ljust(PADDING)
        INFO = "[INFO]".ljust(PADDING)

    @staticmethod
    def print(value, prefix=None):
        print()
        print(str(value).ljust(Output.Prefix.PADDING) if prefix is None else prefix + str(value))


def main():
    try:
        arg_handler = ArgumentHandler(args)
        file_handler = FileHandler(arg_handler.filename)
        board = Board([[cell for cell in row.split()] for row in file_handler.raw_file_content.split("\n")])
        game = Game(board)
        game.start_game()
    except FileNotFoundError as fnf:
        Output.print("{}: {}".format(fnf.strerror, fnf.filename), prefix=Output.Prefix.ERROR)
    except ArgumentHandler.InvalidArgumentException as iae:
        Output.print(iae.message, prefix=Output.Prefix.ERROR)


main()